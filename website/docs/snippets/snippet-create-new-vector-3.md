---
title: New Vector3
---

Create a new Vector3.

## Example

```lua
local newVector = UE.Vector3(0, 0, 0)
```

## User Variables

No user variables.

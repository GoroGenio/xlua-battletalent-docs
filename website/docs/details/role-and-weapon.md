# Role and Weapon code names



#### Below are the characters and weapons available for use in the game(demo version).

![image-20221103092644955](role-and-weapon/image-20221103092644955.png)







| Weapon        | name                                                         |
| ------------- | ------------------------------------------------------------ |
| melee weapons | Arming_Dagger，Arming_Sword，Dagger_Bleed，Falchion_Simple，Falchion_Long，Mace_Simple，Irish_Long_Sword，Rouelle_Dagger，Spear_Simple，Viking_Battle_Axe，Viking_Sword，Goblin_Rogue |


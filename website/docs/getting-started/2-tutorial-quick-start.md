---
title: Quick Start
slug: /
---

## Quick Start

1. Download & Install [Unity Hub](https://unity3d.com/get-unity/download).
2. Download & Install [Unity 2019.4.12](https://unity3d.com/unity/whats-new/2019.4.12).
3. Download [ModProj](https://github.com/fonzieyang/BTModToolkit). (If you don't have git, just click `clone > download ZIP`.)
4. Open the ModProj project in Unity Hub. 
5. Click `BuildTools -> BuildAllBundles` in the toolbar of Unity.
6. When the mod has been built it can be found in the ModProj project location at `ModProj/Assets/Mods`
7. Copy the mod files(mod files are located ) to the corresponding path:
   * To run on Windows: `C:\Users[username]\AppData\LocalLow\CrossLink\BattleTalent\Mods`
   * To run on Quest: `/sdcard/Android/data/com.CrossLink.SAO/files/Mods/`

Done! (if something goes wrong, please open your cheat menu then tell us about your error messages).

## Tutorial videos

If you dont like reading a lot there is also some tutorial videos out there that are easy to follow.

### 10 minutes Tutorial Video

Fonzie made an [official modding tutorial video](https://www.youtube.com/watch?v=alnqZcCeais) to quickly get you going. Click the link!

### Dino's Extended Tutorial Video

Dino, one of our beloved modders made an extensive tutorial which explains stuff in more details. 
[Check it out!](https://www.youtube.com/watch?v=YqETFL-rwc4)

### Prop Tutorial Video

Silver Echo Games Developer, made a tutorial on how to create props.
[Check it out!](https://www.youtube.com/watch?v=sS1CKmxCxcI)

## FAQ

### Does it matter which Unity version I use?
Yes, you should use version 2019.4.12f

### What can we mod right now?
Officially weapons, spells, maps, skins, enemies, friends, traps, songs.
Unofficially we can also mod props for a prop gun spell.
